from os import write
from matplotlib import colors
from sklearn.decomposition import PCA
import numpy as np
from matplotlib import pyplot as plt
from matplotlib.colors import ListedColormap
from sklearn.preprocessing import StandardScaler
import pandas as pd

# Load Data 
x_train = np.load('data/train/imgs.npy')
# y_train = data['y_train']
x_test = np.load('data/test/imgs.npy')
# y_test = data['y_test']

# Ravel Data 
x_train = np.reshape(x_train, newshape=(np.shape(x_train)[0],-1))
x_test = np.reshape(x_train, newshape=(np.shape(x_train)[0],-1))

# scaler = StandardScaler()
# scaler.fit(x_train)
# x_train_scaled = scaler.transform(x_train)

# Fit Train Data with PCA 
pca = PCA(n_components=3)
PC = pca.fit_transform(x_train)
print(pca.explained_variance_)

# Transform Test Data & Add to DF to be plotted 
df1 = pd.DataFrame(pca.transform(x_test), columns=['PCA%i' %i for i in range(3) ])
df1['data_type'] = 'test'

df2 = pd.DataFrame(PC, columns=['PCA%i' %i for i in range(3) ])
df2['data_type'] = 'train'

df = df1.append(df2)
# print(df.head(5))
# print(df.tail(5))

# Plot train and test data
col =df['data_type'].map({'test': 'b', 'train': 'g'})

fig = plt.figure()
ax = plt.axes(projection='3d')
ax.scatter3D(df['PCA0'], df['PCA1'], df['PCA2'], color = col)

xAxis = ((min(df['PCA0']), max(df['PCA0'])), (0,0), (0,0))
ax.plot(xAxis[0], xAxis[1], xAxis[2], 'r')
yAxis = ((min(df['PCA1']), max(df['PCA2'])), (0,0), (0,0))
ax.plot(yAxis[0], yAxis[1], yAxis[2], 'r')
zAxis = ((min(df['PCA2']), max(df['PCA2'])), (0,0), (0,0))
ax.plot(zAxis[0], zAxis[1], zAxis[2], 'r')


plt.show()

